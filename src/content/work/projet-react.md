---
title: Flynaero
publishDate: 2022-08-01 00:00:00
img: /assets/flynaero.png
img_alt: Écran d'accueil du site Flynaero, photo d'un avion Airbus sur fond bleu.
description: |
  Mon premier grand projet en React
tags:
  - Design
  - Développement
  - React
---

Premier grand projet professionnel en language React, un site statique complet et complexe.

Vous pouvez le visiter en suivant ce lien : [Flynaero](https://flynaero.com)
