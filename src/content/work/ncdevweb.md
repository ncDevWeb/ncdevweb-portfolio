---
title: ncDevWeb
publishDate: 2022-11-01 00:00:00
img: /assets/carte-visite_RECTO.png
img_alt: Logo de ncDevWeb, on peut voir 3 lettres v se chevaucher pour former un petit v et un grand W sur fond bleu où des lignes blanches forment des genre d'engrenages.
description: |
  Création d'une micro-entreprise de développement Web
tags:
  - Design
  - Développement
  - Admin Sys
---

En Novembre 2022 je lance ma micro-entreprise de développement Web. Je développe des sites et applications Web destinés au grand public et à des professionnels. Je gère aussi une [instance Mastodon](https://mastodon.devweb.dev) ouverte que j'héberge chez moi. Je compte lancer aussi une instance Peertube pour faire des lives de tutoriels de développement mais aussi héberger les replays.

ncDevWeb s'étend sur plusieurs sites et applications Web de démonstration.
- [devWeb.dev](https://devweb.dev), la plateforme principale développé avec Symfony
- [boutique.devWeb.dev](https://boutique.devweb.dev), la boutique de démonstration développé avec Prestashop.
- [cv.devweb.dev](https://cv.devweb.dev), la démonstration de portfolio ou CV numérique développé avec Astro-build
- [vitrine.devweb.dev](https://vitrine.devweb.dev), la démonstration de site statique détaillé développé avec React
- [mastodon.devweb.dev](https://mastodon.devweb.dev), l'instance Mastodon dédiée au logiciels libre
- À venir, la démonstration de blog développé avec Wordpress et l'instance Peertube hébergée chez moi