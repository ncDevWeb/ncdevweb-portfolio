---
title: Chapelle St Martin
publishDate: 2011-07-01 17:00:00
img: /assets/MSM/MSM-StMartin_enluminure.jpg
img_alt: Deux personnes debouts de dos regardent l'alcôve de la chapelle St Martin où est projeté l'animation d'une enluminure.
description: |
  Mappings et animations dans l'abbaye du Mont Saint Michel
tags:
  - Mapping
  - Annimation
---

Pendant les étés de 2011 à 2014, avec le Kolektif Alambik nous avons élaboré un spectacle de déambulation nocturne dans l'abbaye du Mont Saint Michel.

#### Mapping

 En 2011, les logiciels de mapping étaient rares et peu abordables. Nous avons dû composer avec nos propres moyens. Un travail précis à été nécessaire.

![Image des mesures de l'alcôve de la chappelle St Martin](/assets/MSM/MSM-StMartin_mesures.jpg "mesures de l'alcôve St Martin")

#### Reconstitution
J'ai reconstitué à ma façon ce qu'auraient pû être les peintures qui ornaient les murs de l'abbaye.


![Image des mesures de l'alcôve de la chappelle St Martin](/assets/MSM/MSM-StMartin_peinture.jpg "mesures de l'alcôve St Martin")

#### Animation
Grâce au techniques modernes d'animation, j'ai eu l'honneur de donner vie à des enluminures dantants du Moyen Âge.

> ##### St Michel terrasse le Dragon[^1]
> [![regarder la vidéo](/assets/MSM/MSM-StMartin_dragon.jpg)](https://youtu.be/lhjRzEnpdfc?t=15)
> [^1]: cliquez sur la photo pour voir la vidéo.







