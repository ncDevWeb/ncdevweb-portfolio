---
title: Pete Sputnik
publishDate: 2014-03-28 00:00:00
img: /assets/sputnik.jpg
img_alt: Un homme avec les cheveux rouge en pétard derrière sa batterie verte aux formes originales
description: |
  Réalisation et montage du clip musical de la chanson Nightshuffle de Pete Sputnik
tags:
  - Réalisation
  - Montage
  - Clip
---

Pete Sputnik est une figure de la scène underground chalonnaise. Faisant partie du groupe les cosmoNAutes, Pete est maintenant solo. Il joue de la batteriechante et gère des boucles pré-enregistrées dans un style très personnel. Entre cold-wave, post-punk tribal, electro et techno.

Quand Pete m'a demandé de réaliser son clip, je n'ai pas hésité. Aussi bien pour la musique que pour l'endroit du tournage, le vieux théâtre de Chalon-sur-Saône inscrit aux monuments historiques depuis le 13 mars 1995 (selon Wikipedia).

Après un tournage à toutes vitesses, j'ai monté le clip suivant :

> ##### Pete Sputnik - Nightshuffle[^1]
> [![regarder la vidéo](/assets/pete.webp)](https://youtu.be/dxnQdJzvW9Q)
> [^1]: cliquez sur la photo pour voir la vidéo.
