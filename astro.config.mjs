import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
    env: {
        ADDRESSE_MAIL: process.env.ADDRESSE_MAIL
    }
});
